<?php
/**
 *
 * @package k14theme
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="icon" href="<?= get_template_directory_uri() ?>/assets/img/favicon.png">
	<link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/styles/preloader.css" type="text/css" media="all">
	<script type="text/javascript">
		var Preloader = function() {
			document.querySelector("#preloader").removeAttribute("class");
			document.querySelector("html").classList.remove("fixed");
			document.querySelector("#preloader").style.opacity = "0";
			setTimeout(() => {
				document.querySelector("#preloader").remove();
			}, 5000);
		};
		window.addEventListener('DOMContentLoaded', (event) => Preloader() );
	</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="preloader" class="preloader"><img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" alt="<?= bloginfo('title') ?>"></div>
	<!--[if lt IE 10]>
		<div class="alert alert-warning">
			You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
		</div>
	<![endif]-->
	<?php get_template_part('components/cookies'); ?>
	<div class="wrapper">
		<header class="header header__wrap" id="Header">
			<section class="row container header__container">
				<input type="checkbox" id="navToggler">
				<a href="<?php echo get_home_url() ?>" class="header__logo">
					<img src="<?php echo get_template_directory_uri()?>/assets/img/logo.png" alt="<?= bloginfo('title') ?>">
				</a>
				<label for="navToggler" id="mobile-hamburger" onclick>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</label>
				<nav class="header__nav">
					<?php wp_nav_menu(['theme_location' => 'primary-menu']) ?>
				</nav>
				<label
					for="navToggler"
					id="navCloser"
					class="header__navCloser"
					onclick
				></label>
			</section>
		</header>