import $    from 'jquery';

$(document.body).addClass('under');

$(window).on('scroll', function(){
    if(window.pageYOffset >= $('.curtain').height()){
        $(document.body).removeClass('under');
    }
    else if(!$(document.body).hasClass('under')){
        $(document.body).addClass('under');
    }
})