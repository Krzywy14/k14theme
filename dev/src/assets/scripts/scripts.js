import $					from 'jquery';
import cookies 				from './plugins/cookies';

// import lightbox				from './plugins/lightbox';
// import tabs 				from './plugins/tabs';
// import scrollTop 		from './plugins/scrollTop';
// import userScroll 		from './plugins/scrollStop';
// import anchorNav 			from './plugins/anchorNav';
// import parallax 			from './plugins/parallax';
// import wordSizer 		from './plugins/wordSizer';

$(() => {


	let lastScrollTop = 0;
	$(window).on('scroll', (event) => {
		console.log(event);
		const scrollY = event.currentTarget.scrollY;
		if (scrollY > lastScrollTop && scrollY > 100) {
			$('#Header').addClass('hide');
		} else {
			$('#Header').removeClass('hide');
		}
		lastScrollTop = scrollY;
	});

	$('.wp-block-table').each((_tableIndex, table) => {
		$(table).find('tr').each((rowIndex, rowItem) => {
			if (rowIndex > 0) {
				$(rowItem).find('td').each((tdIndex, tdItem) => {
					$(tdItem).attr('data-tdhead', $($($(table).find('tr')[ 0 ]).find('td')[ tdIndex ]).text());
				});
			}
		});
	});

	// lazy load exemple
	// setTimeout(
	// 	() => import(/* webpackChunkName: 'chunks' */ './_plugins/lightbox').then(lightbox => lightbox.default('.blocks-gallery-item'))
	// 	, 2000);

	const loadBackground = (selector) => {

		// selector must by css class or id and had data-smallbg data-hugebg
		if ($(selector).length > 0) {

			// eslint-disable-next-line no-unused-vars
			$(selector).each((index, item) => {

				if ($(window).width() <= 768 && $(item).attr('data-smallbg')) {
					$(item).css('background-image', `url(${$(item).attr('data-smallbg')})`);

				} else if ($(item).attr('data-hugebg')) {
					$(item).css('background-image', `url(${$(item).attr('data-hugebg')})`);
				}
			});
		}
	};

	$(window).ready(() => {
		loadBackground('.loadBackground');
	});

	$(window).resize(() => {
		loadBackground('.loadBackground');
	});

	// Initialize tabs switcher
	// $(()=>{
	// 	new tabs('.tabs__nav', '.tabs__wrap');
	// });

	// Initialize ScrollTop
	// $(()=>{
	// 	new scrollTop();
	// });

	// Initialize user scroll stop achor, scroll top
	// $(()=>{
	// 	new userScroll();
	// });

	// Initialize anchor navigation
	// $(()=>{
	// 	new anchorNav();
	// });

	// Initialize paralax
	// $(()=>{
	// 	new parallax('#parallax');
	// });

	// Initialize Cokies
	new cookies();
});
