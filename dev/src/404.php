<?php
/**
 *
 * @package k14theme
 */

?>

<?php get_header(); ?>
	<main class="error404">
		<div class="row container error404__wrap">
			<h1 class="error404__title">404</h1>
			<h2 class="error404__content"><?= __( 'Page not found', 'k14theme' ) ?></h2>
			<a
				href="<?php echo get_home_url() ?>"
				class="button button--center error404__button"
			>
				<?= __( 'Back to main page', 'k14theme' ) ?>
			</a>
		</div>
	</main>
<?php get_footer(); ?>