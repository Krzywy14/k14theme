<?php
/**
 *
 * @package k14theme
 */

?>
	<footer class="footer">
		<section class="row container footer__wrap">
			<?php wp_nav_menu(['theme_location' => 'footer-menu']) ?>
		</section>
		<section class="row container copyright__wrap">
			&copy; <?php bloginfo('title'); ?> <?= date("Y"); ?>
		</section>
	</footer>
</div> 
<?php wp_footer(); ?>
<?php if (preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $_SERVER["SERVER_NAME"]) || $_SERVER["SERVER_NAME"] === 'localhost') : ?>
	<script async src='http://<?=$_SERVER["REMOTE_ADDR"]?>:3000/browser-sync/browser-sync-client.js?v=2.26.13'></script>
<?php else : ?>
	<link rel="stylesheet" id="scss-css" href="<?=get_template_directory_uri()?>/assets/styles/main.css" type="text/css" media="all">
<?php endif; ?>
</body>
</html>