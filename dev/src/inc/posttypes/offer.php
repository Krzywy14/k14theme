<?php

// Custom post type offer

if (!function_exists('create_offer_posttype')) {
	function create_offer_posttype() {
		$labels = [
			'name' => __( 'Oferty' ),
			'singular_name' => __( 'Oferta' )
		];
		$args = [
			'labels' 		=> $labels,
			'public' 		=> true,
			'has_archive' 	=> false,
			'hierarchical'	=> true,
			'menu_icon'		=> 'dashicons-yes',
			'supports'		=> ['title','editor','page-attributes'],
			'rewrite' 		=> ['slug' => 'oferta'],
		];
		register_post_type( 'offer', $args);
	}
	add_action( 'init', 'create_offer_posttype' );
}
?>