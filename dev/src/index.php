<?php
/**
 *
 * @package k14theme
 */

?>

<?php get_header(); ?>
	<main class="main">
		<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
			<section class="row container post__wrap">
				<h1 class="post__title">
					<?php the_title(); ?>
				</h1>
				<div class="post__content wp-block__wrap">
					<?php the_content(); ?>
				</div>
			</section>
		<?php endwhile; endif; ?>
	</main>
<?php get_footer(); ?>